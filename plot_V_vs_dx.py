import matplotlib.pyplot as plt
import numpy as np
import math

# 100 linearly spaced numbers
x = np.linspace(420,540,20)

# the function, which is y = x^2 here
# y = (15*x + 5*3**(1.0/2)*abs(- x**2 + 9760*x - 5094400)**(1.0/2) - 19200)/(3*(x - 680)) + ((6000*(3**(1.0/2)*abs(- x**2 + 9760*x - 5094400)**(1.0/2) - 7*x + 2960)*(((7*x + 3040)*(x - 680)*(3**(1.0/2)*abs(- x**2 + 9760*x - 5094400)**(1.0/2) - 7*x + 2960))/(2808*(x**2 - 1360*x + 462400)) - (1381*x)/1404 + 183320/351))/(x - 680)**2)**(1.0/3)


deadzone = 50.0
V_max = 40.0
Cx_frame = 480
a = (math.log(V_max/4 + 1))/deadzone
b = (math.log(V_max/8 + 1))/deadzone

# y = math.exp(a*(x - Cx_frame)) - 1


y1 = []
y2 = []
for i in x:
    if i - Cx_frame > 0:
        y1.append(V_max * (math.exp(a * abs(i - Cx_frame)) - 1))
        y2.append(V_max * (math.exp(b * abs(i - Cx_frame)) - 1))
    else:
        y1.append(V_max * (- math.exp(a * abs(i - Cx_frame)) + 1))
        y2.append(V_max * (- math.exp(b * abs(i - Cx_frame)) + 1))

# print(y)
# setting the axes at the centre
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# plot the function
plt.plot(x,y1, 'r')
plt.plot(x,y2, 'b')

plt.scatter(x,y1)
plt.scatter(x,y2)
# plt.legend()

# show the plot
plt.show()