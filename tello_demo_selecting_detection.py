import os

# comment out below line to enable tensorflow logging outputs
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf

physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
from absl import app, flags, logging
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from core.config import cfg
from PIL import Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
from djitellopy import tello
# deep sort imports
from deep_sort import preprocessing, nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet

deadZone = 75
flightMode = False
tiny = False

iou = 0.45
score = 0.50

weights = './checkpoints/yolov4-416'
if tiny:
    weights = './checkpoints/yolov4-tiny-416'

max_cosine_distance = 0.4
nn_budget = None
nms_max_overlap = 1.0

# initialize deep sort
model_filename = 'model_data/mars-small128.pb'
encoder = gdet.create_box_encoder(model_filename, batch_size=1)
# calculate cosine distance metric
metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
# initialize tracker
tracker = Tracker(metric)

# load configuration for object detector
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

STRIDES = np.array([8, 16, 32])
ANCHORS = np.array(
    [[[12, 16], [19, 36], [40, 28]], [[36, 75], [76, 55], [72, 146]], [[142, 110], [192, 243], [459, 401]]])
NUM_CLASS = 80
XYSCALE = [1.2, 1.1, 1.05]
input_size = 416

saved_model_loaded = tf.saved_model.load(weights, tags=[tag_constants.SERVING])
infer = saved_model_loaded.signatures['serving_default']

drone = tello.Tello()
drone.connect()

drone.for_back_velocity = 0
drone.left_right_velocity = 0
drone.up_down_velocity = 0
drone.yaw_velocity = 0
drone.speed = 0
drone.streamoff()
drone.streamon()

cap = drone.get_frame_read()
vid = drone.get_video_capture()

width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))

X_min = int(width / 2) - deadZone
X_max = int(width / 2) + deadZone
Y_min = int(height / 2) - deadZone
Y_max = int(height / 2) + deadZone

normal_velocity = 40
terminal_velocity = 60

attackMode = [False]
followMode = [False]
selected_object = []
modeOutput = ''

isDetectionPermitted = [True]

maximumDetections = [10]


def click_event(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        # attack mode
        for i in range(len(detections)):
            [x1, y1, x2, y2] = detections[i].to_tlbr()
            if (x1 <= x <= x2) and (y1 <= y <= y2):
                selected_object.append(detections[i])
                # isDetectionPermitted.append(False)
                maximumDetections.append(1)
                attackMode.append(True)
                break
        # for track in tracker.tracks:
        #     if not track.is_confirmed() or track.time_since_update > 1:
        #         continue
        #     bbox = track.to_tlbr()
        #     if (bbox[0] <= x <= bbox[2]) and (bbox[1] <= y <= bbox[3]):
        #         selected_object.append(track)
        #         attackMode.append(True)
        #         break

    if event == cv2.EVENT_RBUTTONDOWN:
        # follow mode
        for i in range(len(detections)):
            [x1, y1, x2, y2] = detections[i].to_tlbr()
            if (x1 <= x <= x2) and (y1 <= y <= y2):
                selected_object.append(detections[i])
                # isDetectionPermitted.append(False)
                maximumDetections.append(1)
                followMode.append(True)
                break
        # for track in tracker.tracks:
        #     if not track.is_confirmed() or track.time_since_update > 1:
        #         continue
        #     bbox = track.to_tlbr()
        #     if (bbox[0] <= x <= bbox[2]) and (bbox[1] <= y <= bbox[3]):
        #         selected_object.append(track)
        #         followMode.append(True)
        #         break

    if event == cv2.EVENT_MBUTTONDOWN:
        # Detect again
        maximumDetections.append(10)
        # isDetectionPermitted.append(True)
        attackMode.append(False)
        followMode.append(False)
        selected_object.clear()
        if flightMode:
            drone.takeoff()


cv2.namedWindow('Tello feed')
cv2.setMouseCallback('Tello feed', click_event)

def move_drone(frame, mode, track):
    bbox = track.to_tlbr()
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1] - 30)),
                  (int(bbox[0]) + (len(class_name) + len(str(track.track_id))) * 17, int(bbox[1])), color, -1)
    w = int(bbox[2]) - int(bbox[0])
    h = int(bbox[3]) - int(bbox[1])
    cx = (int(bbox[0]) + int(bbox[2])) / 2
    cy = (int(bbox[1]) + int(bbox[3])) / 2
    currentArea = w * h

    if mode == 'attack':
        velocity = terminal_velocity
        requiredArea = 0.9 * width * height
    if mode == 'follow':
        velocity = normal_velocity
        requiredArea = 0.1 * width * height

    if cx < X_min:
        drone.yaw_velocity = -1 * normal_velocity
        s_1 = 'Rotate Left'
    if cx > X_max:
        drone.yaw_velocity = 1 * normal_velocity
        s_1 = 'Rotate Right'
    if X_min <= cx <= X_max:
        drone.yaw_velocity = 0
        s_1 = ''
    if cy < Y_min:
        drone.up_down_velocity = 1 * normal_velocity
        s_2 = 'Move Up'
    if cy > Y_max:
        drone.up_down_velocity = -1 * normal_velocity
        s_2 = 'Move Down'
    if Y_min <= cy <= Y_max:
        drone.up_down_velocity = 0
        s_2 = ''
    if currentArea < requiredArea:
        drone.for_back_velocity = velocity
        s_3 = 'Move Forward'
    if currentArea >= requiredArea:
        drone.for_back_velocity = 0
        s_3 = ''
    if drone.send_rc_control:
        drone.send_rc_control(drone.left_right_velocity, drone.for_back_velocity, drone.up_down_velocity,
                              drone.yaw_velocity)

    cv2.putText(frame, s_1 + ' , ' + s_2 + ' , ' + s_3, (int(bbox[0]), int(bbox[1] - 10)), 0, 0.75, (255, 255, 255), 2)

while True:
    if flightMode:
        drone.takeoff()
        flightMode = False

    frame = cap.frame
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(frame)

    image_data = cv2.resize(frame, (input_size, input_size))
    image_data = image_data / 255.
    image_data = image_data[np.newaxis, ...].astype(np.float32)
    start_time = time.time()

    # run detections on tflite if flag is set
    batch_data = tf.constant(image_data)
    pred_bbox = infer(batch_data)
    for key, value in pred_bbox.items():
        boxes = value[:, :, 0:4]
        pred_conf = value[:, :, 4:]

    boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
        boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
        scores=tf.reshape(
            pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
        max_output_size_per_class=maximumDetections[-1],
        max_total_size=50,
        iou_threshold=iou,
        score_threshold=score
    )

    # convert data to numpy arrays and slice out unused elements
    num_objects = valid_detections.numpy()[0]
    bboxes = boxes.numpy()[0]
    bboxes = bboxes[0:int(num_objects)]
    scores = scores.numpy()[0]
    scores = scores[0:int(num_objects)]
    classes = classes.numpy()[0]
    classes = classes[0:int(num_objects)]

    # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
    original_h, original_w, _ = frame.shape
    frameWidth = original_w
    frameHeight = original_h
    bboxes = utils.format_boxes(bboxes, original_h, original_w)

    # store all predictions in one parameter for simplicity when calling functions
    pred_bbox = [bboxes, scores, classes, num_objects]

    # read in all class names from config
    class_names = utils.read_class_names(cfg.YOLO.CLASSES)

    # by default allow all classes in .names file
    # allowed_classes = list(class_names.values())

    # custom allowed classes (uncomment line below to customize tracker for only people)
    allowed_classes = ['person']

    # loop through objects and use class index to get class name, allow only classes in allowed_classes list
    names = []
    deleted_indx = []
    for i in range(num_objects):
        class_indx = int(classes[i])
        class_name = class_names[class_indx]
        if class_name not in allowed_classes:
            deleted_indx.append(i)
        else:
            names.append(class_name)
    names = np.array(names)
    # print(names)
    bboxes = np.delete(bboxes, deleted_indx, axis=0)
    scores = np.delete(scores, deleted_indx, axis=0)

    # encode yolo detections and feed to tracker
    features = encoder(frame, bboxes)
    detections = [Detection(bbox, score, class_name, feature) for bbox, score, class_name, feature in
                  zip(bboxes, scores, names, features)]

    # initialize color map
    cmap = plt.get_cmap('tab20b')
    colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

    # run non-maxima supression
    boxs = np.array([d.tlwh for d in detections])
    scores = np.array([d.confidence for d in detections])
    classes = np.array([d.class_name for d in detections])
    indices = preprocessing.non_max_suppression(boxs, classes, nms_max_overlap, scores)
    detections = [detections[i] for i in indices]
    if len(detections) != 0:
        for i in range(len(detections)):
            [x1, y1, x2, y2] = detections[i].to_tlbr()
            color = colors[int(i+1) % len(colors)]
            color = [i * 255 for i in color]
            cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), color, 2)

    # Call the tracker
    # print(selected_object)
    # if len(selected_object) == 0:


    tracker.predict()
    tracker.update(selected_object)
    # track = tracker.tracks
    print(tracker.tracks)

    for track in tracker.tracks:
        if not track.is_confirmed() or track.time_since_update > 1:
            continue
        print(track)
        bbox = track.to_tlbr()
        cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
        cv2.putText(frame, "Id - " + str(track.track_id),(int(bbox[0]), int(bbox[1]-10)),0, 0.75, (255,255,255),2)
        # if cv2.EVENT_LBUTTONDOWN or cv2.EVENT_RBUTTONDOWN:
        #     break

    if attackMode[-1]:
        modeOutput = 'Attack mode'
        move_drone(frame, 'attack', track)
    if followMode[-1]:
        modeOutput = 'Follow mode'
        move_drone(frame, 'follow', track)

    cv2.putText(frame, "Drone Battery: {}".format(str(drone.get_battery())), (10, 30), 0, 0.75, (255, 255, 255), 2)
    cv2.line(frame, (X_min, Y_min), (X_min, Y_max), (255, 255, 0), 1)
    cv2.line(frame, (X_max, Y_min), (X_max, Y_max), (255, 255, 0), 1)
    cv2.line(frame, (X_min, Y_min), (X_max, Y_min), (255, 255, 0), 1)
    cv2.line(frame, (X_min, Y_max), (X_max, Y_max), (255, 255, 0), 1)

    cv2.putText(frame, modeOutput, (10, 130), 0, 0.75, (255, 255, 255), 2)

    fps = 1.0 / (time.time() - start_time)
    cv2.putText(frame, "FPS: {}".format(str(fps)), (10, 80), 0, 0.75, (255, 255, 255), 2)

    result = np.asarray(frame)
    result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)



    cv2.imshow('Tello feed', result)

    if cv2.waitKey(1) == 27 or 0xFF == ord('q'):
        # drone.yaw_velocity = 20  ## Todo : change accordingly when the object is out of the frame, previous value was = 20
        # drone.up_down_velocity = 5  ## Todo : change accordingly when the object is out of the frame
        time.sleep(60)
        # drone.land()
        break

cv2.destroyAllWindows()
